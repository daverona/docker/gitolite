# docker/gitolite

[![pipeline status](https://gitlab.com/toscana/docker/gitolite/badges/master/pipeline.svg)](https://gitlab.com/toscana/docker/gitolite/commits/master)

* GitLab source repository: [https://gitlab.com/toscana/docker/gitolite](https://gitlab.com/toscana/docker/gitolite)
* Docker Hub repository: [https://hub.docker.com/r/daverona/gitolite](https://hub.docker.com/r/daverona/gitolite)

This is a Docker image for Gitolite on Alpine Linux. This image provides:

* [Gitolite](https://gitolite.com/) 3.6.11
* [OpenSSH](https://www.openssh.com/) 8.0

## Installation

Install [Docker](https://hub.docker.com/search/?type=edition&offering=community) if you dont' have one.
Then pull the image from Docker Hub repository:

```bash
docker image pull daverona/gitolite
```

or build the image:

```bash
docker image build \
  --build-arg TIMEZONE=UTC \
  --tag daverona/gitolite \
  .
```

When you build the image, you can specify a time zone of
Alpine Linux. If you don't specify the value of build argument `TIMEZONE`, UTC will be used.

## Quick Start

Run the container:

```bash
docker container run --rm \
  daverona/gitolite
```

It will show how to set up Gitolite, which
we explain in the next section.

## Usage

### Setting Up Gitolite

To set up Gitolite for the first time:

```bash
docker container run --rm \
  --env ADMIN_PUBLIC_KEY="$(cat ~/.ssh/id_rsa.pub)" \
  --volume /host/path/to/hostkeys:/etc/ssh/keys \
  --volume /host/path/to/repos:/var/lib/git \
  daverona/gitolite
```

The value of `ADMIN_PUBLIC_KEY` should be set Gitolite admin's public key,
which is yours in the above example. You must change this value properly.
The filename of admin's public key is admin.pub.

When set up, you should mount two empty host directories
to the container:

* /host/path/to/hostkeys: directory where the container's HostKey files are kept
* /host/path/to/repos: directory where the repositories are saved

If you don't, the repositories created will be *lost*
when the container terminates.

### Serving Gitolite

Once Gitolite is set up, you can run:

```bash
docker container run --rm \
  --detach \
  --volume /host/path/to/hostkeys:/etc/ssh/keys \
  --volume /host/path/to/repos:/var/lib/git \
  --publish 2022:22 \
  daverona/gitolite
```

Note that `ADMIN_PUBLIC_KEY` is not given.
It will be ignored if given once Gitolite has been set up. You still need to mount the host directories
used for set up though.

To access repositories the default SSH port on the container must bind to
a port on the host, which is 2022 in this example.
Once the container is running, your repository say repo.git can be cloned:

```bash
git clone ssh://git@localhost:2022/repo.git
```

### Administrating Gitolite

If you are the Gitolite admin, you won't do SSH into
the container to do the job. Instead you clone `gitolite-admin.git` and use this repository to do administration. For more information please read
[https://gitolite.com/gitolite/basic-admin.html](https://gitolite.com/gitolite/basic-admin.html).

## References

* [https://gitolite.com/](https://gitolite.com/)
* [https://www.openssh.com/](https://www.openssh.com/)
