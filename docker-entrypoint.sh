#!/bin/sh
set -e

# If the command is sshd, set it up correctly
if [ "${1}" = "/usr/sbin/sshd" ] || [ "${1}" = "sshd" ]; then
  if [ ! -f ~git/.ssh/authorized_keys ] && [ ! -n "$ADMIN_PUBLIC_KEY" ]; then
    # Didn't set up Gitolite yet?
    echo "Welcome to Gitolite!"
    echo
    echo "To set up Gitolite, please mount"
    echo "  /host/path/to/hostkeys (to keep sshd HostKey files)"
    echo "  /host/path/to/repos    (to keep repositories)"
    echo "and pass to the container:"
    echo "  ADMIN_PUBLIC_KEY  (Gitolite admin's ssh public key)"
    echo "For example, run the container:"
    echo
    echo 'docker container run --rm \'
    echo '  --env ADMIN_PUBLIC_KEY="$(cat ~/.ssh/id_rsa.pub)" \'
    echo '  --volume /host/path/to/hostkeys:/etc/ssh/keys \'
    echo '  --volume /host/path/to/repos:/var/lib/git \'
    echo "  daverona/gitolite"
    echo
    echo "If you forget to mount, all repositories will be lost."
    exit 1
  fi

  # Setup SSH HostKeys if needed
  printf "Checking up sshd HostKeys... "
  for algorithm in rsa dsa ecdsa ed25519; do
    key_file=/etc/ssh/keys/ssh_host_${algorithm}_key
    [ -f $key_file ] || ssh-keygen -q -N '' -f $key_file -t $algorithm
    grep -q "HostKey $key_file" /etc/ssh/sshd_config \
      || echo "HostKey $key_file" >> /etc/ssh/sshd_config
  done
  echo "done"

  # Explicitly enable or disable authentications and SFTP subsystem
  printf "Checking up sshd authentications... "
  sed -i -E 's|^#?(PubkeyAuthentication)\s.*|\1 yes|' /etc/ssh/sshd_config
  sed -i -E 's|^#?(HostbasedAuthentication)\s.*|\1 no|' /etc/ssh/sshd_config
  sed -i -E 's|^#?(PasswordAuthentication)\s.*|\1 no|' /etc/ssh/sshd_config
  sed -i -E 's|^#?(ChallengeResponseAuthentication)\s.*|\1 no|' /etc/ssh/sshd_config
  sed -i -E 's|^Subsystem\ssftp\s|#&|' /etc/ssh/sshd_config
  echo "done"

  # Set up Gitolite admin
  if [ ! -f ~git/.ssh/authorized_keys ]; then
    # Set up Gitolite with admin's SSH public key
    echo "Setting up Gitolite..."
    echo "$ADMIN_PUBLIC_KEY" > /tmp/admin.pub
    su - git -c "gitolite setup -pk /tmp/admin.pub"
    rm /tmp/admin.pub
    echo
    echo "Congraturations! Gitolite has been set up."
    echo "From now on you can run the container detached without ADMIN_PUBLIC_KEY."
    echo "You should bind a port on the host to the SSH port (22) on the container"
    echo "to access the repositories via ssh."
    echo "For example, run the container:"
    echo
    echo 'docker container run --rm \'
    echo '  --detach \'
    echo '  --volume /host/path/to/hostkeys:/etc/ssh/keys \'
    echo '  --volume /host/path/to/repos:/var/lib/git \'
    echo '  --publish 22:22 \'
    echo "  image:tag"
    exit 1
  else
    # Check setup at every startup
    printf "Checking up repositories... (be patient, please) "
    chown -R git:git ~git
    su - git -c "gitolite setup"
    echo "done"
    if [ -n "$ADMIN_PUBLIC_KEY" ]; then
      echo "ADMIN_PUBLIC_KEY is ignored since Gitolite has been set up already."
    fi
  fi
  set -- /usr/sbin/sshd -D
  echo "Starting Gitolite..."
fi

exec "$@"
