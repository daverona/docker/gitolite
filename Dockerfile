FROM alpine:3.10

# Setting the system timezone
# @see https://wiki.alpinelinux.org/wiki/Setting_the_timezone
ARG TIMEZONE=UTC
RUN apk add --no-cache tzdata \
  && cp /usr/share/zoneinfo/$TIMEZONE /etc/localtime \
  && echo "$TIMEZONE" > /etc/timezone

# Installing Gitolite and SSH
ARG _VERSION=3.6.11-r0
RUN apk add --no-cache gitolite==$_VERSION openssh \
  && passwd -u git \
  && mkdir -p /etc/ssh/keys

# Configuring entrypoint, ports and working directory
COPY ./docker-entrypoint.sh /
RUN chmod +x /docker-entrypoint.sh
EXPOSE 22/tcp
WORKDIR /var/local

ENTRYPOINT ["/docker-entrypoint.sh"]
CMD ["/usr/sbin/sshd", "-D"]
